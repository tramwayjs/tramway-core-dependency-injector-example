FROM node:onbuild
VOLUME ["/usr/src/app"]
WORKDIR /usr/src/app
ENV NODE_ENV=docker
RUN npm install --silent
EXPOSE 8081
CMD [ "npm", "start" ]