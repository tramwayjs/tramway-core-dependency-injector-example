export default {
    "host": "localhost",
    "port": 8081,
    "path": "model",
    "respondAsText": false,
    "headers": {"content-type": "application/json; charset=utf-8"}
};
